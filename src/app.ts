require('dotenv/config');
require('express-async-errors');

import express from 'express';
import { routes } from './routes/routes';
import { Event, User, UserEvent } from './db/model'
import errorHandler from './config/ErroHandler';
import expressPinoLogger from "express-pino-logger";
import { logger } from './config/AppLogger';
import bcrypt from "bcrypt";
import { AppUtils } from './utils/AppUtils';
import Schedule from './db/model/Schedule';
import { RegisterUserService } from './service';

class App {

    public server: express.Application;
    isDev = process.env.NODE_ENV === 'development'

    constructor() {
        this.server = express();
        this.server.use(expressPinoLogger());
        this.middlewares();
        this.routes();
        this.server.use(errorHandler);
        this.init()
    }

    async init() {
        await this.dbInit();
        await this.populateAdminUsersIfNotExists();
        await this.populateUsersIfNotExists();
    }

    middlewares() {
        this.server.use(express.json());
    }

    routes() {
        this.server.use(routes);
    }

    async dbInit() {
        logger.info("Creating database ...")

        await Event.sync({ alter: this.isDev });
        await User.sync({ alter: this.isDev });
        await Schedule.sync({ alter: this.isDev });
        await UserEvent.sync({ alter: this.isDev });

        logger.info("Database initialized !")
    }

    async populateUsersIfNotExists() {
        const existsDefaultUserInserted = await User.findOne({ where: { fullName: "Lucas Pimentão" } });

        if (!existsDefaultUserInserted) {
            const service = new RegisterUserService();

            logger.info("Inserting default users ...");

            const defaultPassword = "user@123";

            let user01 = {
                fullName: "Lucas Pimentão",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "cagezinho@email.com",
            }

            let user02 = {
                fullName: "Rafael Leão",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "rafaelltm10@hotmail.com"
            }

            let user03 = {
                fullName: "Abner Filipe",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "abner.f.c.r@hotmail.com"
            }

            let user04 = {
                fullName: "Vitor Lamego",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "vitorlamegoo@gmail.com"
            }

            let user05 = {
                fullName: "Nicolas Mantzos",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "georgeos.nicolas83@gmail.com"
            }

            let user06 = {
                fullName: "Ana Aparecida",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "moodenergia@gmail.com"
            }

            let user07 = {
                fullName: "Lorrayne Cardozo",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "lorraynecardozo15@gmail.com"
            }

            let user08 = {
                fullName: "Júlio Altoé",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "juliofioresi@hotmail.com"
            }

            let user09 = {
                fullName: "Daniel Primo",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "danielprimo9@gmail.com"
            }

            let user10 = {
                fullName: "Pedro Campos",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "pedrohenriquec099@gmail.com"
            }

            let user11 = {
                fullName: "Lorrany Souza",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "lorrany_any@hotmail.com"
            }

            let user12 = {
                fullName: "Bianca Sofia ",
                phone: "(61) 99999-9999",
                password: defaultPassword,
                email: "sofiabrasil44@gmail.com"
            }

            await service.execute(user01);
            await service.execute(user02);
            await service.execute(user03);
            await service.execute(user04);
            await service.execute(user05);
            await service.execute(user06);
            await service.execute(user07);
            await service.execute(user08);
            await service.execute(user09);
            await service.execute(user10);
            await service.execute(user11);
            await service.execute(user12);

            logger.info("Admin users inserted...");
        }
    }

    async populateAdminUsersIfNotExists() {
        const existsAdminInserted = await User.findOne({ where: { isAdmin: true } });

        if (!existsAdminInserted) {
            logger.info("Inserting admin users ...");

            const defaultPassword = "york@123";

            let userAdmin01 = {
                fullName: "Admin User 01",
                phone: "",
                password: defaultPassword,
                email: "admin01@email.com",
            }

            let userAdmin02 = {
                fullName: "Admin User 02",
                phone: "",
                password: defaultPassword,
                email: "admin02@email.com"
            }

            await this.saveUserWithPasswordHash(userAdmin01, true);
            await this.saveUserWithPasswordHash(userAdmin02, true);

            logger.info("Admin users inserted...");
        }
    }

    private async saveUserWithPasswordHash(user: any, isAdmin: boolean) {
        const salt = await bcrypt.genSalt(Number(process.env.SALT_ROUNDS));

        await bcrypt
            .hash(user.password, salt)
            .then(async (passwordHashed) => {
                await User.create({
                    id: AppUtils.generateRandomId(),
                    fullName: user.fullName,
                    phone: user.phone,
                    password: passwordHashed,
                    email: user.email,
                    isAdmin: isAdmin
                });
            })
            .catch(_err => {
                logger.error("Error to save admin user")
            });
    }
}

export default new App().server;