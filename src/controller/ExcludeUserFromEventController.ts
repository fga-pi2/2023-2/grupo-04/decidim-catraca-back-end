import { Request, Response, NextFunction } from 'express';
import { ExcludeUserFromEventService } from '../service';
import BaseController from './BaseController';
import { excludeUserFromEventSchema } from './validators/UserSchema';
import { ExcludeUserFromEventRequest } from './request/ExcludeUserFromEventRequest';

export default class ExcludeUserFromEventController extends BaseController {

    public constructor(private readonly service: ExcludeUserFromEventService) { 
        super();
    }

    public execute = async (req: Request, res: Response, _next: NextFunction) => {
        this.validateRequest(excludeUserFromEventSchema, req, res);
        const request: ExcludeUserFromEventRequest = req.body;

        await this.service.execute(request.userId, request.eventId);

        res.status(204).send();
    }
}