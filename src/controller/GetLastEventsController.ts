import { Request, Response, NextFunction } from 'express';
import { GetLastEventsService } from '../service';

export default class GetLastEventsController {

    public constructor(private readonly service: GetLastEventsService) { }

    public execute = async (_req: Request, res: Response, _next: NextFunction) => {
        const response = await this.service.execute();

        res.status(200).send(response);
    }
}