import { ObjectSchema } from 'joi';
import { CreateUserRequest, GetUsersFromEventRequest, LoginUserRequest } from '../request';
import Joi from 'joi';
import { ExcludeUserFromEventRequest } from '../request/ExcludeUserFromEventRequest';

const userRequestSchema: ObjectSchema<CreateUserRequest> = Joi.object().keys({
    fullName: Joi.string().required(),
    email: Joi.string().required(),
    phone: Joi.string().required(),
    password: Joi.string().required(),
})

const loginUserSchema: ObjectSchema<LoginUserRequest> = Joi.object().keys({
    email: Joi.string().required(),
    password: Joi.string().required(),
})

const excludeUserFromEventSchema: ObjectSchema<ExcludeUserFromEventRequest> = Joi.object().keys({
    userId: Joi.string().required(),
    eventId: Joi.number().required(),
})

const getUsersFromEventSchema: ObjectSchema<GetUsersFromEventRequest> = Joi.object().keys({
    eventId: Joi.number().required(),
})

export {
    userRequestSchema,
    loginUserSchema,
    excludeUserFromEventSchema,
    getUsersFromEventSchema
};