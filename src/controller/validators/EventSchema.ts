import { ObjectSchema } from 'joi';
import { CreateEventRequest, EditEventRequest } from '../request';
import Joi from 'joi';

const createEventRequestSchema: ObjectSchema<CreateEventRequest> = Joi.object().keys({
    name: Joi.string().required(),
    date: Joi.date().required(),
    schedule: Joi.array().required(),
    local: Joi.string().required(),
    color: Joi.string().required(),
})

const editEventRequestSchema: ObjectSchema<EditEventRequest> = Joi.object().keys({
    id: Joi.number().required(),
    name: Joi.string().required(),
    date: Joi.date().required(),
    local: Joi.string().required(),
    schedule: Joi.array().required(),
    color: Joi.string().required(),
    isCanceled: Joi.boolean(),
    isRescheduled: Joi.boolean(),
})

export {
    createEventRequestSchema,
    editEventRequestSchema,
};