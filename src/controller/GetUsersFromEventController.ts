import { Request, Response, NextFunction } from 'express';
import { GetUsersFromEventService } from '../service';
import BaseController from './BaseController';

export default class GetUsersFromEventController extends BaseController {

    public constructor(private readonly service: GetUsersFromEventService) {
        super();
    }

    public execute = async (req: Request, res: Response, _next: NextFunction) => {
        const eventId = Number(req.params.id);

        const response = await this.service.execute(eventId);

        res.status(200).send({ users: response });
    }
}