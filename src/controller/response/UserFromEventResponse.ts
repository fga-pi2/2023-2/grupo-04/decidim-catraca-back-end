export interface UserFromEventResponse {
    id: string;
    fullName: string;
    email: string;
    phone: string;
}