import { EventsResponse } from "./EventsResponse";

export interface UsersResponse {
    id: string;
    fullName: string;
    email: string;
    phone: string;
    events: EventsResponse[]
}