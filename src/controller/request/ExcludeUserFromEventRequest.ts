export interface ExcludeUserFromEventRequest {
    eventId: number;
    userId: string;
}  