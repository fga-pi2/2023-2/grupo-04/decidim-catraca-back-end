import { GetUsersFromEventController } from "../../controller";
import { GetUsersFromEventService } from "../../service";

export const getUsersFromEventModule = (): GetUsersFromEventController => {
    const service = new GetUsersFromEventService();
    return new GetUsersFromEventController(service);
}