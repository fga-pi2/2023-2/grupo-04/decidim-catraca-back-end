import { createEventModule } from "./CreateEventModule";
import { editEventModule } from "./EditEventModule";
import { excludeUserFromEventModule } from "./ExcludeUserFromEventModule";
import { getLastEventsModule } from "./GetLastEventsModule";
import { getUsersFromEventModule } from "./GetUsersFromEventModule";
import { getUsersModule } from "./GetUsersModule";
import { loginModule } from "./LoginModule";
import { registerUserModule } from "./RegisterUserModule";

export {
    loginModule,
    registerUserModule,
    createEventModule,
    editEventModule,
    getUsersModule,
    getLastEventsModule,
    excludeUserFromEventModule,
    getUsersFromEventModule
}