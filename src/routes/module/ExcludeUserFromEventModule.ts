import { ExcludeUserFromEventController } from "../../controller";
import { ExcludeUserFromEventService } from "../../service";

export const excludeUserFromEventModule = (): ExcludeUserFromEventController => {
    const service = new ExcludeUserFromEventService();
    return new ExcludeUserFromEventController(service);
}