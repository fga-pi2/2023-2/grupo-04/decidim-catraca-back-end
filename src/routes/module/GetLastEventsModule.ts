import { GetLastEventsController } from "../../controller";
import { GetLastEventsService } from "../../service";

export const getLastEventsModule = (): GetLastEventsController => {
    const service = new GetLastEventsService();
    return new GetLastEventsController(service);
}