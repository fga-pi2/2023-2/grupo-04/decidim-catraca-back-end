import { Router } from "express";
import { authAdmin, authUser } from "../config/auth";
import { createEventModule, editEventModule, excludeUserFromEventModule, getLastEventsModule, getUsersFromEventModule, getUsersModule, loginModule, registerUserModule } from "./module";
import { getEventsModule } from "./module/GetEventsModule";
import { getSingleUserModule } from "./module/GetSingleUserModule";

const routes = Router();


routes.get('/events', authUser, getEventsModule().execute);
routes.get('/user', authUser, getSingleUserModule().execute);
routes.get('/users', authAdmin, getUsersModule().execute);
routes.get('/last-events', authUser, getLastEventsModule().execute);
routes.get('/users/events/:id', authAdmin, getUsersFromEventModule().execute);

routes.post('/login', loginModule().execute);
routes.post('/signup', registerUserModule().execute);
routes.post('/events', authAdmin, createEventModule().execute);

routes.put('/events', authAdmin, editEventModule().execute);

routes.delete('/users/events', authAdmin, excludeUserFromEventModule().execute);

export { routes };
