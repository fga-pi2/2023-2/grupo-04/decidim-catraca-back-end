import { EventsResponseMapper } from "../controller/mapper";
import { EventsResponse, UsersResponse } from "../controller/response";
import { User, UserEvent, Event, Schedule } from "../db/model"

export default class GetUsersService {
    public execute = async () => {
        const response: UsersResponse[] = [];

        const users = await User.findAll({
            where: {
                isAdmin: false
            }
        });

        for(const user of users) {
            let eventsResponse: EventsResponse[] = [];

            const userEvents = await UserEvent.findAll({
                where: {
                    user_id: user.id
                }
            });

            for(const userEvent of userEvents){
                const event = await Event.findOne({
                    where: {
                        id: userEvent.event_id
                    },
                });

                const schedules = await Schedule.findAll({
                    where: {
                        eventId: event!.id!
                    }
                });

                eventsResponse.push(EventsResponseMapper.from(event!, schedules));
            }
            
            response.push({
                id: user.id,
                fullName: user.fullName,
                email: user.email,
                phone: user.phone,
                events: eventsResponse
            });
        }

        return response;
    }
}