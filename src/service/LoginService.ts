import ErrorResponse from "../config/ErrorResponse";
import { User } from "../db/model";
import bcrypt from "bcrypt";
import { LoginUserRequest } from "../controller/request";
import jwt from 'jsonwebtoken';
import { LoginUserResponse } from "../controller/response";

export default class LoginService {
    public execute = async (userRequest: LoginUserRequest): Promise<LoginUserResponse> => {
        let userValid: boolean = false;
        let userFound: any;
        await User.findOne({where: {email: userRequest.email }}).then(async userData => {
            userFound = userData;
            if(userFound){
                await bcrypt
                    .compare(userRequest.password, userFound.password)
                    .then(res => {
                        userValid = res
                    })
                    .catch(_err => {
                        throw new ErrorResponse(400,"Error to find user! Try again later!")
                    }) 
            }
            
        }).catch(_err =>{ throw new ErrorResponse(503,"The server is unavailable to handle this request right now.") });

        if(!userValid || !userFound){
            throw new ErrorResponse(404,"User or password invalid!");
        }

        const token = jwt.sign({ id: userFound.id, email: userFound.email, isAdmin: userFound.isAdmin }, process.env.SECRET_JWT!, {
            expiresIn: '2 days',
        });

        return {fullName: userFound.fullName, id: userFound.id, isAdmin: userFound.isAdmin, token}
    }
}