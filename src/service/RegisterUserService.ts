import ErrorResponse from "../config/ErrorResponse";
import { User, UserEvent } from "../db/model";
import bcrypt from "bcrypt";
import { CreateUserRequest } from "../controller/request";
import { AppUtils } from "../utils/AppUtils";
import { Event } from "../db/model"
import { Op } from "sequelize";

export default class RegisterUserService {
    public execute = async (user: CreateUserRequest) => {
        let userResponse: any;
        const userId = AppUtils.generateRandomId();

        await User.findOne({ where: { email: user.email } }).then(userData => {
            userResponse = userData;
        }).catch(err => { throw new ErrorResponse(503, "The server is unavailable to handle this request right now.", err) });

        if (!userResponse) {
            const salt = await bcrypt.genSalt(Number(process.env.SALT_ROUNDS));
            await bcrypt
                .hash(user.password, salt)
                .then(async (passwordHashed) => {
                    user.password = passwordHashed;
                    await User.create({
                        id: userId,
                        fullName: user.fullName,
                        phone: user.phone,
                        password: user.password,
                        email: user.email
                    });

                    const currentDate = new Date();

                    const events = await Event.findAll({
                        where: {
                            date: {
                                [Op.gte]: currentDate,
                            },
                        },
                    });

                    for (const event of events) {
                        await UserEvent.create({
                            event_id: event.id!,
                            user_id: userId
                        })
                    }
                })
                .catch(_err => {
                    throw new ErrorResponse(400, "Error to save user! Try again later!")
                });
        }
        else {
            throw new ErrorResponse(409, "User already registered!");
        }
    }
}