import ErrorResponse from "../config/ErrorResponse";
import { CreateEventRequest } from "../controller/request";
import { Event, User, UserEvent } from "../db/model";
import Schedule from "../db/model/Schedule";

export default class CreateEventService {
    public execute = async (event: CreateEventRequest) => {
        try {
            const eventToSave = {
                name: event.name,
                date: event.date,
                local: event.local,
                color: event.color,
            }
            const eventSaved = await Event.create(eventToSave);

            for (const schedule of event.schedule) {
                const scheduleToSave = {
                    eventId: eventSaved.id!,
                    ...schedule
                }
                await Schedule.create(scheduleToSave);
            }

            const allUsers = await User.findAll();

            for(const user of allUsers){
                await UserEvent.create({
                    event_id: eventSaved.id!,
                    user_id: user.id
                })
            }
        } catch (error) {
            throw new ErrorResponse(400, "Error to create event", error);
        }

    }
}