import ErrorResponse from "../config/ErrorResponse";
import { User, UserEvent } from "../db/model"
import { Event } from "../db/model"

export default class ExcludeUserFromEventService {
    public execute = async (userId: string, eventId: number) => {
        const user = await User.findOne({
            where: {
                id: userId
            }
        });

        if (!user) {
            throw new ErrorResponse(404, "User not found !");
        }

        const event = await Event.findOne({
            where: {
                id: eventId
            }
        });

        if (!event) {
            throw new ErrorResponse(404, "Event not found !");
        }

        await UserEvent.destroy({
            where: {
                user_id: userId,
                event_id: eventId
            }
        })
    }
}