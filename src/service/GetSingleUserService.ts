import ErrorResponse from "../config/ErrorResponse";
import { EventsResponseMapper } from "../controller/mapper";
import { EventsResponse } from "../controller/response";
import { Schedule, User, UserEvent, Event } from "../db/model"

export default class GetSingleUserService {
    public execute = async (userId: string) => {
        let eventsResponse: EventsResponse[] = [];
        
        const user = await User.findOne({
            where: {
                id: userId
            }
        });

        if (!user) {
            throw new ErrorResponse(404, "User not found !");
        }

        const userEvents = await UserEvent.findAll({
            where: {
                user_id: user.id
            }
        });

        for(const userEvent of userEvents){
            const event = await Event.findOne({
                where: {
                    id: userEvent.event_id
                },
            });

            const schedules = await Schedule.findAll({
                where: {
                    eventId: event!.id!
                }
            });

            eventsResponse.push(EventsResponseMapper.from(event!, schedules));
        }

        return {
            id: user.id,
            fullName: user.fullName,
            email: user.email,
            phone: user.phone,
            events: eventsResponse
        }
    }
}