import ErrorResponse from "../config/ErrorResponse";
import { UserFromEventResponse } from "../controller/response";
import { User, UserEvent } from "../db/model"
import { Event } from "../db/model"
export default class GetUsersFromEventService {
    public execute = async (eventId: number) => {
        let response: UserFromEventResponse[] = [];
        const event = await Event.findOne({
            where: {
                id: eventId
            }
        });

        if (!event) {
            throw new ErrorResponse(404, "Event not found !");
        }

        const userEvents = await UserEvent.findAll({
            where: {
                event_id: eventId
            }
        })

        for (const userEvent of userEvents) {
            const user = await User.findOne({
                where: {
                    id: userEvent.user_id
                }
            })

            response.push({
                id: user!.id,
                fullName: user!.fullName,
                email: user!.email,
                phone: user!.phone,
            })
        }

        return response;
    }
}