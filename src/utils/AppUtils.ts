import AppConstants from "../config/AppConstants";

export class AppUtils {
    public static dateAbbreviationFormatter(date: Date) {
        const months = [
            'Jan.', 'Feb.', 'Mar.', 'Apr.', 'May', 'Jun.', 'Jul.', 'Aug.', 'Sep.', 'Oct.', 'Nov.', 'Dec.'
        ];

        const formattedMonth = months[date.getMonth()];
        const formattedDay = date.getDate();

        const dayAbbreviations = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];
        const dayOfWeekIndex = date.getDay();
        const dayAbbreviation = dayAbbreviations[dayOfWeekIndex];

        return `${dayAbbreviation} ${formattedDay} ${formattedMonth}`;
    }

    public static generateRandomId(): string {
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        let id = '';

        for (let i = 0; i < AppConstants.NUMBER_MAX_ID; i++) {
            const randomIndex = Math.floor(Math.random() * characters.length);
            id += characters.charAt(randomIndex);
        }

        return id;
    }
}