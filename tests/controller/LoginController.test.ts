import { Request, Response, NextFunction } from 'express';
import LoginController from '../../src/controller/LoginController';
import { LoginService } from '../../src/service';
import { LoginUserRequest } from '../../src/controller/request/LoginUserRequest';

jest.mock('../../src/service/LoginService');

describe('LoginController', () => {
  let loginService: LoginService;
  let controller: LoginController;
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;
  let mockNext: NextFunction;

  beforeEach(() => {
    loginService = {
      execute: jest.fn(),
    } as unknown as LoginService;

    controller = new LoginController(loginService);

    mockRequest = {};
    mockResponse = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };
    mockNext = jest.fn();
  });

  it('should validate request and send response', async () => {
    const mockUser: LoginUserRequest = {
      email: 'test@example.com',
      password: 'password123',
    };

    mockRequest.body = mockUser;

    await controller.execute(mockRequest as Request, mockResponse as Response, mockNext);

    expect(loginService.execute).toHaveBeenCalledWith(mockUser);
    expect(loginService.execute).toHaveBeenCalledTimes(1);
    expect(mockResponse.status).toHaveBeenCalledWith(200);
    expect(mockNext).not.toHaveBeenCalled();
  });

  it('should handle validation errors', async () => {
    const loginController = new LoginController(loginService);

    const mockUser: Partial<LoginUserRequest> = {};

    mockRequest.body = mockUser;

    await expect(loginController.execute(mockRequest as Request, mockResponse as Response, mockNext)).rejects.toEqual({error: null, status: 422, message: '\"email\" is required'});
  });
});