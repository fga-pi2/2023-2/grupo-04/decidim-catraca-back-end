import GetSingleUserController from '../../src/controller/GetSingleUserController';
import { GetSingleUserService } from '../../src/service';
import { UsersResponse } from '../../src/controller/response';
import jwt from 'jsonwebtoken';

describe('GetSingleUserController', () => {
  let getSingleUserController: GetSingleUserController;
  let getSingleUserServiceMock: jest.Mocked<GetSingleUserService>;
  let req: any;
  let res: any;
  let next: jest.Mock;

  beforeEach(() => {
    getSingleUserServiceMock = {
      execute: jest.fn(),
    } as jest.Mocked<GetSingleUserService>;

    getSingleUserController = new GetSingleUserController(getSingleUserServiceMock);

    req = {
      header: jest.fn(),
    } as any;

    res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    } as any;

    next = jest.fn();
  });

  it('should get single user with valid token', async () => {
    const token = 'validToken';
    const decodedUser = { id: '1' } as any;

    req.header.mockReturnValueOnce(`Bearer ${token}`);
    jest.spyOn(jwt, 'verify').mockReturnValueOnce(decodedUser);

    const mockUsersResponse: UsersResponse = {
      id: '1',
      fullName: 'John Doe',
      email: 'john.doe@example.com',
      phone: '123-456-7890',
      events: [],
    };

    getSingleUserServiceMock.execute.mockResolvedValueOnce(mockUsersResponse);

    await getSingleUserController.execute(req, res, next);

    expect(req.header).toHaveBeenCalledWith('Authorization');
    expect(jwt.verify).toHaveBeenCalledWith(token, process.env.SECRET_JWT);
    expect(getSingleUserServiceMock.execute).toHaveBeenCalledWith(decodedUser.id);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.send).toHaveBeenCalledWith({ user: mockUsersResponse });
    expect(next).not.toHaveBeenCalled();
  });

  it('should handle invalid token and call next', async () => {
    const invalidToken = 'invalidToken';
    const invalidTokenError = new Error('Invalid Token');

    req.header.mockReturnValueOnce(`Bearer ${invalidToken}`);
    jest.spyOn(jwt, 'verify').mockImplementationOnce(() => {
      throw invalidTokenError;
    });

    await expect(getSingleUserController.execute(req, res, next)).rejects.toEqual(invalidTokenError);
  });
});
