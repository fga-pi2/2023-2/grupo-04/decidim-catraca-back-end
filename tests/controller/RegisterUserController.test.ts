// RegisterUserController.test.ts
import { Request, Response, NextFunction } from 'express';
import RegisterUserController from '../../src/controller/RegisterUserController';
import RegisterUserService from '../../src/service/RegisterUserService';

describe('RegisterUserController', () => {
  let mockService: RegisterUserService;
  let controller: RegisterUserController;
  let mockRequest: Partial<Request>;
  let mockResponse: Partial<Response>;
  let mockNext: NextFunction;

  beforeEach(() => {
    mockService = {
      execute: jest.fn(),
    } as unknown as RegisterUserService;

    controller = new RegisterUserController(mockService);

    mockRequest = {
      body: {
        fullName: 'teste',
        email: 'teste@email.com',
        phone: '9999-9999',
        password: 'password123',
      },
    };

    mockResponse = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    };

    mockNext = jest.fn();
  });

  it('should create a new user successfully', async () => {
    await controller.execute(mockRequest as Request, mockResponse as Response, mockNext);

    expect(mockResponse.status).toHaveBeenCalledWith(201);
    expect(mockResponse.send).toHaveBeenCalledWith({ message: 'User created!' });
    expect(mockService.execute).toHaveBeenCalledWith(mockRequest.body);
  });

  it('should handle validation error', async () => {
    mockRequest.body = {};

    await expect(controller.execute(mockRequest as Request, mockResponse as Response, mockNext)).rejects.toEqual({error: null, status: 422, message: '\"fullName\" is required'});
  });
});