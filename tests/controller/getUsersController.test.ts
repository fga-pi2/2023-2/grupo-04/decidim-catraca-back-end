import { NextFunction } from 'express';
import GetUsersController from '../../src/controller/GetUsersController';
import { GetUsersService } from '../../src/service';
import { UsersResponse } from '../../src/controller/response';

describe('GetUsersController', () => {
  let getUsersController: GetUsersController;
  let getUsersServiceMock: jest.Mocked<GetUsersService>;
  let req: any;
  let res: any;
  let next: NextFunction;

  beforeEach(() => {
    getUsersServiceMock = {
      execute: jest.fn(),
    } as jest.Mocked<GetUsersService>;

    getUsersController = new GetUsersController(getUsersServiceMock);

    req = {} as any;
    res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    } as any;

    next = jest.fn();
  });

  it('should call service and send response', async () => {
    const mockUsersResponse: UsersResponse[] = [
      {
        id: '1',
        fullName: 'John Doe',
        email: 'john.doe@example.com',
        phone: '123-456-7890',
        events: [],
      },
    ];

    getUsersServiceMock.execute.mockResolvedValueOnce(mockUsersResponse);

    await getUsersController.execute(req, res, next);

    expect(getUsersServiceMock.execute).toHaveBeenCalled();
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.send).toHaveBeenCalledWith({ users: mockUsersResponse });
    expect(next).not.toHaveBeenCalled();
  });

  it('should handle service error and call next', async () => {
    const error = new Error('Service Error');
    getUsersServiceMock.execute.mockRejectedValueOnce(error);

    await expect(getUsersController.execute(req, res, next)).rejects.toEqual(error);
  });
});