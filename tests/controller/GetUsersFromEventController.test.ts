import { NextFunction } from 'express';
import GetUsersFromEventController from '../../src/controller/GetUsersFromEventController';
import { GetUsersFromEventService } from '../../src/service';
import { UserFromEventResponse } from '../../src/controller/response';

describe('GetUsersFromEventController', () => {
  let getUsersFromEventController: GetUsersFromEventController;
  let getUsersFromEventServiceMock: jest.Mocked<GetUsersFromEventService>;
  let req: any;
  let res: any;
  let next: NextFunction;

  beforeEach(() => {
    getUsersFromEventServiceMock = {
      execute: jest.fn(),
    } as jest.Mocked<GetUsersFromEventService>;

    getUsersFromEventController = new GetUsersFromEventController(getUsersFromEventServiceMock);

    req = {
      params: {
        id: '1',
      },
    } as any;

    res = {
      status: jest.fn().mockReturnThis(),
      send: jest.fn(),
    } as any;

    next = jest.fn();
  });

  it('should call service and send response', async () => {
    const mockUsersResponse: UserFromEventResponse[] = [
      {
        id: '1',
        fullName: 'John Doe',
        email: 'john.doe@example.com',
        phone: '123-456-7890',
        inEvent: true,
      },
    ];

    getUsersFromEventServiceMock.execute.mockResolvedValueOnce(mockUsersResponse);

    await getUsersFromEventController.execute(req, res, next);

    expect(getUsersFromEventServiceMock.execute).toHaveBeenCalledWith(1);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.send).toHaveBeenCalledWith({ users: mockUsersResponse });
    expect(next).not.toHaveBeenCalled();
  });

  it('should handle service error and call next', async () => {
    const error = new Error('Service Error');
    getUsersFromEventServiceMock.execute.mockRejectedValueOnce(error);

    await expect(getUsersFromEventController.execute(req, res, next)).rejects.toEqual(error);
  });
});