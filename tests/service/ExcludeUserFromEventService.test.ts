import { EditEventRequest } from "../../src/controller/request";
import {ExcludeUserFromEventService } from "../../src/service";
import {Event, User, UserEvent } from "../../src/db/model";
import ErrorResponse from "../../src/config/ErrorResponse";

jest.mock('../../src/db/model/Event');
jest.mock('../../src/db/model/Schedule');
jest.mock('../../src/db/model/User');
jest.mock('../../src/db/model/UserEvent');

describe('ExcludeUserFromEventService', () => {
  let excludeUserFromEventService: ExcludeUserFromEventService;

  beforeEach(() => {
    excludeUserFromEventService = new ExcludeUserFromEventService();
  });

  it('should exclude user from event successfully', async () => {
    
    const userId = 'user123';
    const eventId = 1;

   
    jest.spyOn(User, 'findOne').mockResolvedValueOnce({ id: userId } as User);
    jest.spyOn(Event, 'findOne').mockResolvedValueOnce({ id: eventId } as Event);

    
    jest.spyOn(UserEvent, 'destroy').mockResolvedValueOnce(1);

    await excludeUserFromEventService.execute(userId, eventId);

    expect(User.findOne).toHaveBeenCalledWith({
      where: { id: userId },
    });

    expect(Event.findOne).toHaveBeenCalledWith({
      where: { id: eventId },
    });

    expect(UserEvent.destroy).toHaveBeenCalledWith({
      where: { user_id: userId, event_id: eventId },
    });
  });

  it('should handle user not found', async () => {
    const userId = 'nonexistentuser';
    const eventId = 1;

    jest.spyOn(User, 'findOne').mockResolvedValueOnce(null);

    let error: any;
    try {
      await excludeUserFromEventService.execute(userId, eventId);
    } catch (err) {
      error = err;
    }

    expect(error).toStrictEqual(
      new ErrorResponse(404, 'User not found !', error.error)
    );
  });

  it('should handle event not found', async () => {
    const userId = 'user123';
    const eventId = 999;

    jest.spyOn(User, 'findOne').mockResolvedValueOnce({ id: userId } as User);
    jest.spyOn(Event, 'findOne').mockResolvedValueOnce(null);

    let error: any;
    try {
      await excludeUserFromEventService.execute(userId, eventId);
    } catch (err) {
      error = err;
    }

    expect(error).toStrictEqual(
      new ErrorResponse(404, 'Event not found !', error.error)
    );
  });
});
