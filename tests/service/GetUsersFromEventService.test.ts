import GetUsersFromEventService from '../../src/service/GetUsersFromEventService';
import { UserFromEventResponse } from '../../src/controller/response';
import { User, UserEvent, Event } from '../../src/db/model';

jest.mock('../../src/db/model', () => ({
  User: {
    findAll: jest.fn(),
  },
  Event: {
    findOne: jest.fn(),
  },
  UserEvent: {
    findAll: jest.fn(),
  }
}));

describe('GetUsersFromEventService', () => {
  let getUsersFromEventService: GetUsersFromEventService;

  beforeEach(() => {
    getUsersFromEventService = new GetUsersFromEventService();
  });

  it('should get users from event', async () => {
    const eventId = 1;

    const mockEvent = {
      id: eventId,
      name: 'Event 1',
    };

    const mockUsers = [
      { id: 1, fullName: 'John Doe', email: 'john.doe@example.com', phone: '123-456-7890' },
      { id: 2, fullName: 'Jane Doe', email: 'jane.doe@example.com', phone: '987-654-3210' },
    ];

    const mockUserEvents = [
      { user_id: 1, event_id: eventId },
      { user_id: 2, event_id: eventId },
    ];

    (Event.findOne as jest.Mock).mockResolvedValueOnce(mockEvent);
    (User.findAll as jest.Mock).mockResolvedValueOnce(mockUsers);
    (UserEvent.findAll as jest.Mock).mockResolvedValueOnce(mockUserEvents);

    const result: UserFromEventResponse[] = await getUsersFromEventService.execute(eventId);

    expect(result).toEqual([
      {
        id: 1,
        fullName: 'John Doe',
        email: 'john.doe@example.com',
        phone: '123-456-7890',
        inEvent: true,
      },
      {
        id: 2,
        fullName: 'Jane Doe',
        email: 'jane.doe@example.com',
        phone: '987-654-3210',
        inEvent: true,
      },
    ]);
  });

  it('should handle event not found', async () => {
    const eventId = 1;

    (Event.findOne as jest.Mock).mockResolvedValueOnce(null);

    await expect(getUsersFromEventService.execute(eventId)).rejects.toEqual({error: null, message: 'Event not found !', status: 404});
  });
});