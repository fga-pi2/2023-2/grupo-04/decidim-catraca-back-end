import LoginService from '../../src/service/LoginService';
import { User } from '../../src/db/model';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { LoginUserRequest } from '../../src/controller/request';

jest.mock('../../src/db/model', () => ({
  User: {
    findOne: jest.fn(),
  },
}));

jest.mock('bcrypt', () => ({
  compare: jest.fn(),
}));

jest.mock('jsonwebtoken', () => ({
  sign: jest.fn(),
}));

describe('LoginService', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should successfully login and return user data with token', async () => {
    const mockedUser = {
      id: 1,
      email: 'test@example.com',
      password: '$2b$10$UyOPW4QYBjHnGcbKFOO24eF5bwFExFp2yfKMRUDDxB2EqW7PTeFAS', // bcrypt hashed password
      fullName: 'Test User',
      isAdmin: false,
    };

    (User.findOne as jest.Mock).mockResolvedValue(mockedUser);

    (bcrypt.compare as jest.Mock).mockResolvedValue(true);

    (jwt.sign as jest.Mock).mockReturnValue('mockedToken');

    const loginService = new LoginService();
    const userRequest: LoginUserRequest = {
      email: 'test@example.com',
      password: 'password123',
    };

    const result = await loginService.execute(userRequest);

    expect(User.findOne).toHaveBeenCalledWith({ where: { email: userRequest.email } });
    expect(bcrypt.compare).toHaveBeenCalledWith(userRequest.password, mockedUser.password);
    expect(jwt.sign).toHaveBeenCalledWith(
      { id: mockedUser.id, email: mockedUser.email, isAdmin: mockedUser.isAdmin },
      process.env.SECRET_JWT!,
      { expiresIn: '2 days' }
    );

    expect(result).toEqual({
      fullName: mockedUser.fullName,
      id: mockedUser.id,
      isAdmin: mockedUser.isAdmin,
      token: 'mockedToken',
    });
  });

  it('should throw ErrorResponse when user is not found', async () => {
    (User.findOne as jest.Mock).mockResolvedValue(null);

    const loginService = new LoginService();
    const userRequest: LoginUserRequest = {
      email: 'nonexistent@example.com',
      password: 'password123',
    };

    await expect(loginService.execute(userRequest)).rejects.toEqual({"error": null, "message": "User or password invalid!", "status": 404});
  });
});