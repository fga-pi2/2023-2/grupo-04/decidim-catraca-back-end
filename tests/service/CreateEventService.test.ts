import ErrorResponse from "../../src/config/ErrorResponse";
import { CreateEventRequest } from "../../src/controller/request";
import { Schedule, User, UserEvent, Event } from "../../src/db/model";
import { CreateEventService } from "../../src/service";


jest.mock('../../src/db/model/Event');
jest.mock('../../src/db/model/Schedule');
jest.mock('../../src/db/model/User');
jest.mock('../../src/db/model/UserEvent');

describe('CreateEventService', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should create event successfully', async () => {
    
    const userMock1= {id: "1", fullName: 'teste1', email: 'teste@email', phone: '99999', password: 'string', isAdmin: false};
    const userMock2 = {id: "2", fullName: 'teste2', email: 'teste@email', phone: '99999', password: 'string', isAdmin: false};
    const mockEventCreate = jest.spyOn(Event, 'create').mockResolvedValue({ id: 1 });
    const mockScheduleCreate = jest.spyOn(Schedule, 'create').mockResolvedValue({});
    const mockUserFindAll = jest.spyOn(User, 'findAll').mockResolvedValue([userMock1, userMock2] as any);
    const mockUserEventCreate = jest.spyOn(UserEvent, 'create').mockResolvedValue({});

   
    const createEventService = new CreateEventService();

    
    const eventRequest: CreateEventRequest = {
      name: 'Sample Event',
      date: new Date('2023-12-05'),
      local: 'Sample Location',
      color: '#000000',
      schedule: [{ startTime: '09:00', endTime: '10:00' } as any],
    };

    
    await createEventService.execute(eventRequest);

    
    expect(mockEventCreate).toHaveBeenCalledWith({
      name: eventRequest.name,
      date: eventRequest.date,
      local: eventRequest.local,
      color: eventRequest.color,
    });

    expect(mockScheduleCreate).toHaveBeenCalledWith({
      eventId: 1,
      ...eventRequest.schedule[0],
    });

    expect(mockUserFindAll).toHaveBeenCalled();

    expect(mockUserEventCreate).toHaveBeenCalledTimes(2); 

    
  });

  it('should handle errors and throw ErrorResponse', async () => {
   
    const mockEventCreate = jest.spyOn(Event, 'create').mockRejectedValue(new Error('Mocked error'));

   
    const createEventService = new CreateEventService();

   
    const eventRequest: CreateEventRequest = {
      name: 'Sample Event',
      date: new Date('2023-12-05'),
      local: 'Sample Location',
      color: '#000000',
      schedule: [{ startTime: '09:00', endTime: '10:00' } as any],
    };

    let error: any;
    try {
      await createEventService.execute(eventRequest);
    } catch (err) {
      error = err;
    }

    expect(error).toStrictEqual(
      new ErrorResponse(400, "Error to create event", error.error)
    );
  });
});