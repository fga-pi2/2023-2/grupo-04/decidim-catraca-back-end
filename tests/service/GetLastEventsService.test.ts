import { Op } from "sequelize";
import { Schedule, Event } from "../../src/db/model";
import { GetLastEventsService } from "../../src/service";

jest.mock('../../src/db/model/Event');
jest.mock('../../src/db/model/Schedule');
jest.mock('../../src/db/model/User');
jest.mock('../../src/db/model/UserEvent');

describe('GetLastEventsService', () => {
  let getEventsService: GetLastEventsService;

  beforeEach(() => {
    getEventsService = new GetLastEventsService();
  });

  it('should get last events and map them successfully', async () => {
    
    const currentDate = new Date("2023-05-7");
    const events: Array<Event> = [
      { id: 1, name: "teste", date: currentDate, local: 'teste', color:'teste',isCanceled:false, isRescheduled:false } as Event,
      { id: 2, name: "teste", date: currentDate, local: 'teste', color:'teste',isCanceled:false, isRescheduled:false } as Event,
    ];

   
    jest.spyOn(Event, 'findAll').mockResolvedValueOnce(events as Array<Event>);
    jest.spyOn(Schedule, 'findAll').mockResolvedValue([]);
    const today = new Date("2023-10-7");
    const currentDateSpy = jest
      .spyOn(global, "Date")
      .mockImplementation(() => today);

    
    const result = await getEventsService.execute();
    console.log(result.data[0].events[0])
   
    expect(result).toEqual({
      data: [
        {
          date: 'SUN 7 May',
          events: [
            { id: 1, name: "teste", date: currentDate, schedule: [], local: 'teste', color:'teste'},
            { id: 2, name: "teste", date: currentDate, schedule: [], local: 'teste', color:'teste'},
          ],
        },
      ],
    });

   
    expect(Event.findAll).toHaveBeenCalledWith({
      where: {
        date: {
          [Op.lt]: today,
        },
      },
      order: [['date', 'DESC']],
    });

  });
});
