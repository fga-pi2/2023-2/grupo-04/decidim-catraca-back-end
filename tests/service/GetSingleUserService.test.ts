import GetSingleUserService from '../../src/service/GetSingleUserService';
import { Schedule, User, UserEvent, Event } from '../../src/db/model';
import { EventsResponseMapper } from '../../src/controller/mapper';

jest.mock('../../src/db/model', () => ({
  User: {
    findOne: jest.fn(),
  },
  Event: {
    findOne: jest.fn(),
  },
  UserEvent: {
    findAll: jest.fn(),
  },
  Schedule: {
    findAll: jest.fn(),
  },
}));

jest.mock('../../src/controller/mapper');

describe('GetSingleUserService', () => {
  let getSingleUserService: GetSingleUserService;

  const date = Date.now();

  beforeEach(() => {
    jest.resetAllMocks();
    getSingleUserService = new GetSingleUserService();
  });

  it('should get single user with events', async () => {
    const userId = '1';

    const mockUser = {
      id: userId,
      fullName: 'John Doe',
      email: 'john.doe@example.com',
      phone: '123-456-7890',
    };

    const mockUserEvents = [
      { user_id: userId, event_id: 101 },
      { user_id: userId, event_id: 102 },
    ];

    const mockEvent1 = {
      id: 101,
      name: 'Event 1',
      date: date,
      schedules: [{
        scheduleEventId: 201, 
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      }],
      local: 'teste',
      color: 'green',
    };

    const mockEvent2 = {
      id: 102,
      name: 'Event 2',
      date: date,
      schedules: [{ 
        scheduleEventId: 202,
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      }],
      local: 'teste',
      color: 'green',
    };

    const mockSchedules = [
      {
        scheduleEventId: 201, 
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      },
      { 
        scheduleEventId: 202,
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      },
    ];

    (User.findOne as jest.Mock).mockResolvedValueOnce(mockUser);
    (UserEvent.findAll as jest.Mock).mockResolvedValueOnce(mockUserEvents);
    (Event.findOne as jest.Mock).mockResolvedValueOnce(mockEvent1).mockResolvedValueOnce(mockEvent2);
    (Schedule.findAll as jest.Mock).mockResolvedValueOnce(mockSchedules);

    (EventsResponseMapper.from as jest.Mock).mockReturnValueOnce({
      id: 101,
      name: 'Event 1',
      date: date,
      schedules: [{
        scheduleEventId: 201, 
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      }],
      local: 'teste',
      color: 'green',
    }).mockReturnValueOnce({
      id: 102,
      name: 'Event 2',
      date: date,
      schedules: [{
        scheduleEventId: 202, 
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      }],
      local: 'teste',
      color: 'green',
    });

    const result = await getSingleUserService.execute(userId);

    expect(result).toEqual({
      id: '1',
      fullName: 'John Doe',
      email: 'john.doe@example.com',
      phone: '123-456-7890',
      events: [
        {
          id: 101,
          name: 'Event 1',
          date: date,
          schedules: [{
            scheduleEventId: 201, 
            startTime: '2023-01-01T12:00:00',
            endTime: '2023-01-01T12:00:00',
            description: 'teste teste teste',
          }],
          local: 'teste',
          color: 'green',
        },
        {
          id: 102,
          name: 'Event 2',
          date: date,
          schedules: [{ 
            scheduleEventId: 202,
            startTime: '2023-01-01T12:00:00',
            endTime: '2023-01-01T12:00:00',
            description: 'teste teste teste',
          }],
          local: 'teste',
          color: 'green',
        },
      ],
    },);
  });

  it('should handle user not found', async () => {
    const userId = '1';
    (User.findOne as jest.Mock).mockResolvedValueOnce(null);

    await expect(getSingleUserService.execute(userId)).rejects.toEqual({error: null, message: 'User not found !', status: 404});
  });
});