import RegisterUserService from '../../src/service/RegisterUserService';
import { User } from '../../src/db/model';
import bcrypt from 'bcrypt';
import { CreateUserRequest } from '../../src/controller/request';
import { AppUtils } from '../../src/utils/AppUtils';

jest.mock('../../src/db/model/User');
jest.mock('bcrypt');
jest.mock('../../src/utils/AppUtils');

describe('RegisterUserService', () => {
  let registerUserService: RegisterUserService;
  let mockUser: CreateUserRequest;

  beforeEach(() => {
    registerUserService = new RegisterUserService();
    mockUser = {
      fullName: 'John Doe',
      phone: '123456789',
      email: 'john@example.com',
      password: 'password123',
    };
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should successfully register a new user', async () => {
    (User.findOne as jest.Mock).mockResolvedValueOnce(null);

    (bcrypt.genSalt as jest.Mock).mockResolvedValueOnce('mockedSalt');
    (bcrypt.hash as jest.Mock).mockResolvedValueOnce('password123');

    (AppUtils.generateRandomId as jest.Mock).mockReturnValueOnce('mockedUserId');

    (User.create as jest.Mock).mockResolvedValueOnce({});

    await expect(registerUserService.execute(mockUser)).resolves.toBeUndefined();

    expect(User.findOne).toHaveBeenCalledWith({ where: { email: mockUser.email } });
    expect(bcrypt.genSalt).toHaveBeenCalledWith(Number(process.env.SALT_ROUNDS));
    expect(bcrypt.hash).toHaveBeenCalledWith(mockUser.password, "mockedSalt");
    expect(AppUtils.generateRandomId).toHaveBeenCalled();
    expect(User.create).toHaveBeenCalledWith({
      id: 'mockedUserId',
      fullName: mockUser.fullName,
      phone: mockUser.phone,
      password: 'password123',
      email: mockUser.email,
    });
  });

  it('should throw an error if the user already exists', async () => {
    (User.findOne as jest.Mock).mockResolvedValueOnce({
      id: '1',
      fullName: 'John Doe',
      phone: '123456789',
      email: 'john@example.com',
      password: 'password123',
    });

    await expect(registerUserService.execute(mockUser)).rejects.toEqual({error: null, message: "User already registered!", status: 409});

    expect(User.findOne).toHaveBeenCalledWith({ where: { email: mockUser.email } });
  });
});