import events from "events";
import { Op } from "sequelize";
import { EventsResponseMapper } from "../../src/controller/mapper";
import { Schedule, Event } from "../../src/db/model";
import { GetEventsService } from "../../src/service";
import { AppUtils } from "../../src/utils/AppUtils";

jest.mock('../../src/db/model/Event');
jest.mock('../../src/db/model/Schedule');
jest.mock('../../src/db/model/User');
jest.mock('../../src/db/model/UserEvent');

describe('GetEventsService', () => {
  let getEventsService: GetEventsService;

  beforeEach(() => {
    getEventsService = new GetEventsService();
  });

  it('should get events and map them successfully', async () => {
    
    const currentDate = new Date("2023-11-7");
    const events: Array<Event> = [
      { id: 1, name: "teste", date: currentDate, local: 'teste', color:'teste',isCanceled:false, isRescheduled:false } as Event,
      { id: 2, name: "teste", date: currentDate, local: 'teste', color:'teste',isCanceled:false, isRescheduled:false } as Event,
    ];

   
    jest.spyOn(Event, 'findAll').mockResolvedValueOnce(events as Array<Event>);
    jest.spyOn(Schedule, 'findAll').mockResolvedValue([]);
    const today = new Date("2023-10-7");
    const currentDateSpy = jest
      .spyOn(global, "Date")
      .mockImplementation(() => today);

    
    const result = await getEventsService.execute();
    console.log(result.data[0].events[0])
   
    expect(result).toEqual({
      data: [
        {
          date: 'TUE 7 Nov.',
          events: [
            { id: 1, name: "teste", date: currentDate, schedule: [], local: 'teste', color:'teste'},
            { id: 2, name: "teste", date: currentDate, schedule: [], local: 'teste', color:'teste'},
          ],
        },
      ],
    });

   
    expect(Event.findAll).toHaveBeenCalledWith({
      where: {
        date: {
          [Op.gte]: today,
        },
      },
      order: [['date', 'DESC']],
    });

  });
});
