import GetUsersService from '../../src/service/GetUsersService';
import { UsersResponse } from '../../src/controller/response';
import { User, UserEvent, Event, Schedule } from '../../src/db/model';
import { EventsResponseMapper } from '../../src/controller/mapper';

jest.mock('../../src/db/model');
jest.mock('../../src/controller/mapper');

describe('GetUsersService', () => {
  beforeEach(() => {
    jest.resetAllMocks();
  });

  it('should get users with events and schedules', async () => {

    const date = Date.now();
    
    const mockUser = {
      id: 1,
      fullName: 'John Doe',
      email: 'john.doe@example.com',
      phone: '123-456-7890',
    };

    const mockUserEvents = [
      { user_id: '1', event_id: 101 },
      { user_id: '1', event_id: 102 },
    ];

    const mockEvent1 = {
      id: 101,
      name: 'Event 1',
      date: date,
      schedules: [{
        scheduleEventId: 201, 
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      }],
      local: 'teste',
      color: 'green',
    };

    const mockEvent2 = {
      id: 102,
      name: 'Event 2',
      date: date,
      schedules: [{ 
        scheduleEventId: 202,
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      }],
      local: 'teste',
      color: 'green',
    };

    const mockSchedules = [
      {
        scheduleEventId: 201, 
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      },
      { 
        scheduleEventId: 202,
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      },
    ];

    // Mock database queries
    (User.findAll as jest.Mock).mockResolvedValueOnce([mockUser]);
    (UserEvent.findAll as jest.Mock).mockResolvedValueOnce(mockUserEvents);
    (Event.findOne as jest.Mock).mockResolvedValueOnce(mockEvent1).mockResolvedValueOnce(mockEvent2);
    (Schedule.findAll as jest.Mock).mockResolvedValueOnce(mockSchedules);

    (EventsResponseMapper.from as jest.Mock).mockReturnValueOnce({
      id: 101,
      name: 'Event 1',
      date: date,
      schedules: [{
        scheduleEventId: 201, 
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      }],
      local: 'teste',
      color: 'green',
    }).mockReturnValueOnce({
      id: 102,
      name: 'Event 2',
      date: date,
      schedules: [{
        scheduleEventId: 202, 
        startTime: '2023-01-01T12:00:00',
        endTime: '2023-01-01T12:00:00',
        description: 'teste teste teste',
      }],
      local: 'teste',
      color: 'green',
    });

    const getUsersService = new GetUsersService();
    const result: UsersResponse[] = await getUsersService.execute();

    expect(result).toEqual([
      {
        id: 1,
        fullName: 'John Doe',
        email: 'john.doe@example.com',
        phone: '123-456-7890',
        events: [
          {
            id: 101,
            name: 'Event 1',
            date: date,
            schedules: [{
              scheduleEventId: 201, 
              startTime: '2023-01-01T12:00:00',
              endTime: '2023-01-01T12:00:00',
              description: 'teste teste teste',
            }],
            local: 'teste',
            color: 'green',
          },
          {
            id: 102,
            name: 'Event 2',
            date: date,
            schedules: [{ 
              scheduleEventId: 202,
              startTime: '2023-01-01T12:00:00',
              endTime: '2023-01-01T12:00:00',
              description: 'teste teste teste',
            }],
            local: 'teste',
            color: 'green',
          },
        ],
      },
    ]);
  });
});
