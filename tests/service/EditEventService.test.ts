import { EditEventRequest } from "../../src/controller/request";
import { EditEventService } from "../../src/service";
import { Schedule,Event } from "../../src/db/model";
import ErrorResponse from "../../src/config/ErrorResponse";


jest.mock('../../src/db/model/Event');
jest.mock('../../src/db/model/Schedule');
jest.mock('../../src/db/model/User');
jest.mock('../../src/db/model/UserEvent');

describe('EditEventService', () => {
  let editEventService: EditEventService;

  beforeEach(() => {
    editEventService = new EditEventService();
  });

  it('should successfully edit an event', async () => {
   
    const editEventRequest: EditEventRequest = {
      id: 1,
      name: 'Edited Event',
      date: new Date(),
      local: 'Edited Location',
      color: '#0000FF',
      isCanceled: true,
      isRescheduled: false,
      schedule: [{ startTime: '09:00', endTime: '10:00' } as any],
    };

    
    const spyEventUpdate = jest.spyOn(Event, 'update');
    const spyScheduleUpdate = jest.spyOn(Schedule, 'update');

  
    await editEventService.execute(editEventRequest);

   
    expect(Event.update).toHaveBeenCalledWith(
      expect.objectContaining({
        id: editEventRequest.id,
        name: editEventRequest.name,
        date: editEventRequest.date,
        local: editEventRequest.local,
        color: editEventRequest.color,
        isCanceled: editEventRequest.isCanceled,
        isRescheduled: editEventRequest.isRescheduled,
      }),
      expect.any(Object)
    );

    for (const schedule of editEventRequest.schedule) {
      expect(Schedule.update).toHaveBeenCalledWith(
        expect.objectContaining({
          eventId: editEventRequest.id,
          ...schedule,
        }),
        expect.any(Object)
      );
    }
  });

  it('should handle errors when editing an event', async () => {
   
    const editEventRequest: EditEventRequest = {
      id: 1,
      name: 'Edited Event',
      date: new Date(),
      local: 'Edited Location',
      color: '#0000FF',
      isCanceled: true,
      isRescheduled: false,
      schedule:  [{ startTime: '09:00', endTime: '10:00' } as any],
    };

   
    const spyEventUpdate = jest.spyOn(Event, 'update').mockRejectedValueOnce(new Error('Database error'));
    let error: any;
    try {
      await editEventService.execute(editEventRequest);
    } catch (err) {
      error = err;
    }

    expect(error).toStrictEqual(
        new ErrorResponse(400, "Error to edit event", error.error)
    );
    
  });
});
